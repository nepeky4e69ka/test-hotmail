import { EventEmitter } from 'events';
import { ImapFlow } from 'imapflow';

export interface Credential {
  id: string;
  user: string;
  pass: string;
  proxy: string;
}

export interface RecieverInstance extends EventEmitter {
  credential: Credential;
  client: ImapFlow;
  run: () => void;
  logout: () => void;
}

export interface Recievers {
  [id: string]: RecieverInstance;
}

export interface Log {
  date?: number;
  id: string;
  message: string;
}

export enum Event {
  LOG = 'log',
  UPDATE = 'exists',
  ERROR = 'error',
  DIE = 'SIGTERM',
}
