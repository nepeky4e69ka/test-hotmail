import { Recievers, Credential, Log, Event as E } from './interfaces';
import { maxLines, fileFullPath } from './config';
import { Reciever } from './reciever';
import { readFileSync } from 'fs';
import 'colors';
// eslint-disable-next-line
const logUpdate = require('log-update');

class Watcher {
  private credentials: Credential[] = [];
  private recievers: Recievers = {};
  private logs: Log[] = [];
  /**
   * @description create instance for each credential
   */
  public async run() {
    this.credentials = await this.getCredentials();
    for (const credential of this.credentials) {
      this.recievers[credential.id] = new Reciever(credential);
      this.recievers[credential.id].on(E.LOG, (log: Log) => this.addLog(log));
      this.recievers[credential.id].run();
    }
    this.addStartup();
    this.interval();
  }
  /**
   * @description add log to console
   */
  private addLog(newLog: Log) {
    this.logs.push(newLog);
    if (this.logs.length > maxLines) {
      this.logs.shift();
    }
  }
  /**
   * @description wait for new logs
   */
  private interval() {
    setInterval(() => {
      const logArray = this.logs.map((log: Log) => this.lts(log));
      const logString = logArray.join('\n');
      logUpdate(logString);
    }, 1000);
  }
  /**
   * @description logout on process exit
   */
  public logout() {
    for (const key in this.recievers) {
      this.recievers[key].logout();
    }
  }
  /**
   * @description add first log
   */
  private addStartup() {
    const len = this.credentials.length;
    this.addLog({
      id: 'Run',
      message: `recievers (${len})`,
    });
  }
  /**
   *
   * @function lts log to string
   * @returns string
   */
  private lts({ date, message, id }: Log): string {
    const ts = date || new Date();
    const timeString = `${new Date(ts).toLocaleTimeString()}:`.gray;
    const idString = `${id}`.white;
    const msgString = `${message}`.green;
    return `${timeString} ${idString} # ${msgString}`;
  }
  /**
   *
   * @description load credentials from json
   */
  private getCredentials(): Promise<Credential[]> {
    return new Promise((resolve) => {
      let result: Credential[] = [];
      try {
        const dataString = readFileSync(fileFullPath, 'utf-8');
        result = JSON.parse(dataString);
      } catch (e) {
        void e;
      }
      resolve(result);
    });
  }
}

const watcher = new Watcher();
process.on(E.DIE, () => watcher.logout());
console.clear();
watcher.run();
