import 'dotenv/config';
import { resolve } from 'path';
export const host = process.env.DEFAULT_HOST as string;
export const port = parseInt(process.env.DEFAULT_PORT as string) || 993;
export const maxLines = parseInt(process.env.LINES as string) || 5;
export const digits = parseInt(process.env.DIGITS as string) || 6;
export const filePath = process.env.FILE_PATH as string;
export const fileFullPath = resolve(process.cwd(), filePath);
export const regex = new RegExp(`[0-9]{${digits}}`, 'g');
export const imap = {
  host,
  port,
  secure: true,
  logger: false as any,
  tls: {
    rejectUnauthorized: false,
  },
};
export const options = {
  flags: true,
  envelope: true,
  source: true,
};
