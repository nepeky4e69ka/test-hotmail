import { Credential, Event as E, RecieverInstance as RI } from './interfaces';
import { imap, regex, options } from './config';
import { simpleParser } from 'mailparser';
import { EventEmitter } from 'events';
import { ImapFlow } from 'imapflow';

export class Reciever extends EventEmitter implements RI {
  public client!: ImapFlow;
  constructor(public credential: Credential) {
    super();
  }
  /**
   * @description create mail listener
   */
  public async run() {
    const { user, pass, proxy } = this.credential;
    const auth = { user, pass };
    this.client = new ImapFlow({ auth, ...imap }); // proxy
    this.client.on(E.ERROR, (err) => this.sendLog(err));
    this.client.on(E.UPDATE, () => this.handleUpdate());
    await this.client.connect();
    await this.client.getMailboxLock('INBOX');
    await this.client.idle();
  }
  /**
   * @description fetch new envelope
   */
  private async handleUpdate(): Promise<void> {
    for await (const { envelope, source } of this.client.fetch('*', options)) {
      const { text } = await simpleParser(source);
      const date = new Date(envelope.date).getTime();
      const matcher = (text as string).match(regex);
      if (matcher) {
        this.sendLog(matcher[0], date);
      }
    }
  }
  private sendLog(message: string, date?: number): void {
    const { id } = this.credential;
    this.emit(E.LOG, {
      message,
      date,
      id,
    });
  }
  public async logout(): Promise<void> {
    await this.client.logout();
  }
}
